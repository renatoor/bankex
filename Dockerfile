FROM elixir:alpine

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN apk add build-base
RUN apk add make
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix do compile
CMD mix ecto.create && \
    mix ecto.migrate && \
    mix run priv/repo/seeds.exs && \
    mix phx.server
