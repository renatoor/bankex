# Bankex

A simple banking system where users can create an account with an initial balance of $1000.00 to withdraw and transfer money.

## Links

[API documentation](https://dear-lightpink-canary.gigalixirapp.com/docs)

[Modules documentation](https://renatoor.gitlab.io/bankex/)

## Workflow

1. [Sign-up](https://dear-lightpink-canary.gigalixirapp.com/docs#/user/BankexWeb.UserController.create)
2. [Login using email or cpf](https://dear-lightpink-canary.gigalixirapp.com/docs#/user/BankexWeb.UserController.login)
3. [Show](https://dear-lightpink-canary.gigalixirapp.com/docs#/user/BankexWeb.UserController.index) or [update](https://dear-lightpink-canary.gigalixirapp.com/docs#/user/BankexWeb.UserController.update) user
4. [Show account information](https://dear-lightpink-canary.gigalixirapp.com/docs#/account/BankexWeb.AccountController.index)
5. [Withdraw](https://dear-lightpink-canary.gigalixirapp.com/docs#/account/BankexWeb.AccountController.withdraw) or [transfer](https://dear-lightpink-canary.gigalixirapp.com/docs#/account/BankexWeb.AccountController.transfer) money
6. [Show account history](https://dear-lightpink-canary.gigalixirapp.com/docs#/history/BankexWeb.HistoryController.index)

## Backoffice report

Backoffice users can generate a report of the total amount of money transactioned in the bank at a given period.
By default an admin user is created with the following credentials:

- **Email:** admin@bankex.com
- **Password:** backoffice#admin

## Backoffice workflow

1. [Login](https://dear-lightpink-canary.gigalixirapp.com/docs#/backoffice_user/BankexWeb.Backoffice.UserController.login)
2. [Show](https://dear-lightpink-canary.gigalixirapp.com/docs#/backoffice_user/BankexWeb.Backoffice.UserController.index) or [update](https://dear-lightpink-canary.gigalixirapp.com/docs#/backoffice_user/BankexWeb.Backoffice.UserController.update) backoffice user
3. [Generate report](https://dear-lightpink-canary.gigalixirapp.com/docs#/backoffice_report/BankexWeb.Backoffice.ReportController.index)

## Database schema

![Database schema](https://i.imgur.com/P9YQEBk.png)

New entries are inserted on the history table using an update and insert trigger on the accounts table.

## Local setup

Docker is required.

```bash
docker-compose build
docker-compose up
```

[Insomnia workspace export used for testing.](https://gitlab.com/renatoor/bankex/-/blob/master/Insomnia-All_2021-02-23.json)

## 
