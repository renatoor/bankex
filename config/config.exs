# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bankex,
  ecto_repos: [Bankex.Repo]

# Configures the endpoint
config :bankex, BankexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TkjkIEwdb6+WKCqsShUGZamzFDCtbsexMTrDjxVGmmbO4ZO2CEZ6MvpMtgHR0eVt",
  render_errors: [view: BankexWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Bankex.PubSub,
  live_view: [signing_salt: "dyRtnLaj"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures Guardian
config :bankex, Bankex.Accounts.Guardian,
  issuer: "bankex",
  secret_key: System.get_env("BACKOFFICE_SECRET") || "backoffice-secret-key"

# Configures Guardian
config :bankex, Bankex.Backoffice.Guardian,
  issuer: "bankex",
  secret_key: System.get_env("ACCOUNTS_SECRET") || "accounts-secret-key"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
