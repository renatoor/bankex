defmodule Bankex.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Ecto.Changeset
  alias Bankex.Repo

  alias Bankex.Accounts.User
  alias Bankex.Accounts.Account
  alias Bankex.Accounts.History

  alias Argon2

  @initial_balance 100_000

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id),
    do:
      Repo.one!(
        from u in User,
          join: a in assoc(u, :account),
          where: u.id == ^id,
          preload: [account: a]
      )

  @doc """
  Gets a single user by account number.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user_by_account!(123)
      %User{}

      iex> get_user_by_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_by_account!(number),
    do:
      Repo.one!(
        from u in User,
          join: a in assoc(u, :account),
          where: a.number == ^number,
          preload: [account: a]
      )

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(user_attrs \\ %{}, agency) when is_integer(agency) do
    %User{}
    |> User.changeset(user_attrs)
    |> Changeset.put_assoc(:account, %Account{agency: agency, balance: @initial_balance})
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Authenticate a user.

  ## Examples

      iex> authenticate_user(credential, password)
      {:ok, %User{}}

      iex> authenticate_user(credential, password)
      {:error, :invalid_credentials}

  """
  def authenticate_user(credential, password) do
    query = from u in User, where: u.email == ^credential or u.cpf == ^credential

    case Repo.one(query) do
      nil ->
        Argon2.no_user_verify()
        {:error, :invalid_credentials}

      user ->
        if Argon2.verify_pass(password, user.password) do
          {:ok, user}
        else
          {:error, :invalid_credentials}
        end
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{data: %Account{}}

  """
  def change_account(%Account{} = account, attrs \\ %{}) do
    Account.changeset(account, attrs)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account(123)
      %Account{}

      iex> get_account(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(number), do: Repo.get_by!(Account, number: number)

  @doc """
  Withdraw money from account

  ## Examples

      iex> withdraw(account, amount)
      {:ok, %User{}}

      iex> withdraw(account, amount)
      {:error, %Ecto.Changeset{}}

  """
  def withdraw(%Account{} = account, amount) when is_integer(amount) do
    Repo.update(change_account(account, %{balance: account.balance - amount}))
  end

  @doc """
  Transfer money from one account to another

  ## Examples

      iex> transfer(from, to, amount)
      {:ok, %User{}}

      iex> transfer(from, to, amount)
      {:error, %Ecto.Changeset{}}

  """
  def transfer(%Account{} = from, %Account{} = to, amount) when is_integer(amount) do
    Repo.transaction(fn ->
      Repo.update!(change_account(from, %{balance: from.balance - amount}))
      Repo.update!(change_account(to, %{balance: to.balance + amount}))
    end)
  end

  @doc """
  Returns the list of account history logs.

  ## Examples

      iex> get_history(account_id)
      [%History{}, ...]

  """
  def get_history(account_id),
    do:
      Repo.all(
        from h in History,
          where: h.account_id == ^account_id,
          order_by: [desc: :inserted_at]
      )

  @doc """
  Returns the list of account history logs filtered by date.

  ## Examples

      iex> get_history_by_date(account_id)
      [%History{}, ...]

  """
  def get_history_by_date(account_id, begin_date, end_date),
    do:
      Repo.all(
        from h in History,
          where:
            h.account_id == ^account_id and h.inserted_at >= ^begin_date and
              h.inserted_at <= ^end_date,
          order_by: [desc: :inserted_at]
      )

  @doc """
  Returns amount sum given operation(debit or credit).

  ## Examples

      iex> get_history_total(:debit)
      10000

  """
  def get_history_total(operation),
    do:
      Repo.one!(
        from h in History,
          where: h.operation == ^operation,
          select: sum(h.amount)
      )

  @doc """
  Returns amount sum given operation(debit or credit) by account.

  ## Examples

      iex> get_history_total(account_id, :debit)
      10000

  """
  def get_history_total(account_id, operation),
    do:
      Repo.one!(
        from h in History,
          where: h.account_id == ^account_id and h.operation == ^operation,
          select: sum(h.amount)
      )

  @doc """
  Returns amount sum given operation(debit or credit) filtered by date.

  ## Examples

      iex> get_history_total_by_date(:debit, begin_date, end_date)
      10000

  """
  def get_history_total_by_date(operation, begin_date, end_date),
    do:
      Repo.one!(
        from h in History,
          where:
            h.operation == ^operation and h.inserted_at >= ^begin_date and
              h.inserted_at <= ^end_date,
          select: sum(h.amount)
      )

  @doc """
  Returns amount sum given operation(debit or credit) by account filtered by date.

  ## Examples

      iex> get_history_total_by_date(account_id, :debit, begin_date, end_date)
      10000

  """
  def get_history_total_by_date(account_id, operation, begin_date, end_date),
    do:
      Repo.one!(
        from h in History,
          where:
            h.account_id == ^account_id and h.operation == ^operation and
              h.inserted_at >= ^begin_date and h.inserted_at <= ^end_date,
          select: sum(h.amount)
      )
end
