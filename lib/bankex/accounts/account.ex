defmodule Bankex.Accounts.Account do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias Bankex.Accounts.User

  schema "accounts" do
    field :agency, :integer
    field :balance, :integer
    field :digit, :integer, default: 0
    field :number, :integer, read_after_writes: true

    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:agency, :number, :digit, :balance, :user_id])
    |> validate_required([:agency, :balance, :user_id])
    |> foreign_key_constraint(:user_id)
  end
end
