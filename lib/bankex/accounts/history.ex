defmodule Bankex.Accounts.History do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  schema "history" do
    field :amount, :integer
    field :current_balance, :integer
    field :operation, Ecto.Enum, values: [:debit, :credit]
    field :previous_balance, :integer
    field :account_id, :id

    timestamps(updated_at: false)
  end

  @doc false
  def changeset(history, attrs) do
    history
    |> cast(attrs, [:operation, :previous_balance, :current_balance, :amount])
    |> validate_required([:operation, :previous_balance, :current_balance, :amount])
  end
end
