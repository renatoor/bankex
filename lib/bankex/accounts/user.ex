defmodule Bankex.Accounts.User do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset
  import CPF.Ecto.Changeset, only: [validate_cpf: 2]

  alias Argon2

  alias Bankex.Accounts.Account

  schema "users" do
    field :cpf, :string
    field :email, :string
    field :name, :string
    field :password, :string

    field :old_password, :string, virtual: true

    has_one :account, Account

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :email, :cpf, :password])
    |> validate_required([:name, :email, :cpf, :password])
    |> unique_constraint(:email)
    |> unique_constraint(:cpf)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 8)
    |> validate_cpf(:cpf)
    |> put_password_hash
  end

  @doc false
  def update_changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :email, :cpf, :password, :old_password])
    |> validate_required([:name, :email, :cpf, :password])
    |> unique_constraint(:email)
    |> unique_constraint(:cpf)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 8)
    |> validate_cpf(:cpf)
    |> verify_old_password
    |> put_password_hash
  end

  defp verify_old_password(
         %Ecto.Changeset{valid?: true, changes: %{old_password: old_password}, data: user} =
           changeset
       ) do
    if Argon2.verify_pass(old_password, user.password) do
      change(changeset, old_password: nil)
    else
      add_error(changeset, :old_password, "invalid password")
    end
  end

  defp verify_old_password(
         %Ecto.Changeset{valid?: true, changes: %{password: _password}} = changeset
       ) do
    validate_required(changeset, :old_password)
  end

  defp verify_old_password(changeset), do: changeset

  defp put_password_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset
       ) do
    change(changeset, password: Argon2.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset), do: changeset
end
