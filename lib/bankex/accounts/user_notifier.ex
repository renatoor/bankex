defmodule Bankex.Accounts.UserNotifier do
  @moduledoc false
  # For simplicity, this module simply logs messages to the terminal.
  # You should replace it by a proper email or notification tool, such as:
  #
  #   * Swoosh - https://hexdocs.pm/swoosh
  #   * Bamboo - https://hexdocs.pm/bamboo
  #
  require Logger

  defp deliver(body) do
    Logger.debug(body)
  end

  def deliver_withdraw_notice(user, amount) do
    deliver("""
    from: "Bankex" <no-reply@bankex.com>
    to: #{user.email}
    subject: "Withdrawal confirmation"

    ==============================

    Withdrawal confirmation of #{amount} from your Bankex account.

    Agency: #{user.account.agency}
    Account: #{user.account.number}-#{user.account.digit}

    If you do not recognize this operation please contact us at support@bankex.com.

    ==============================
    """)
  end

  def deliver_transfer_notice(from, to, amount) do
    deliver("""
    from: "Bankex" <no-reply@bankex.com>
    to: #{from.name} <#{from.email}>
    subject: "Transfer confirmation"

    ==============================

    Transfer confirmation of #{amount} to the following Bankex account.

    Name: #{to.name}
    Agency: #{to.account.agency}
    Account: #{to.account.number}-#{to.account.digit}

    If you do not recognize this operation please contact us at support@bankex.com.

    ==============================
    """)

    deliver("""
    from: "Bankex" <no-reply@bankex.com>
    to: #{to.email}
    subject: "Transfer received"

    ==============================

    You received a transfer of #{amount} from #{from.name}.

    ==============================
    """)
  end
end
