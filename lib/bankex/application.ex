defmodule Bankex.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Bankex.Repo,
      # Start the Telemetry supervisor
      BankexWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Bankex.PubSub},
      # Start the Endpoint (http/https)
      BankexWeb.Endpoint
      # Start a worker by calling: Bankex.Worker.start_link(arg)
      # {Bankex.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Bankex.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BankexWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
