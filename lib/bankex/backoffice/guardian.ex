defmodule Bankex.Backoffice.Guardian do
  @moduledoc false
  use Guardian, otp_app: :bankex

  alias Bankex.Backoffice

  def subject_for_token(user, _claims) do
    {:ok, to_string(user.id)}
  end

  def resource_from_claims(%{"sub" => id}) do
    user = Backoffice.get_user!(id)
    {:ok, user}
  rescue
    Ecto.NoResultsError -> {:error, :resource_not_found}
  end
end
