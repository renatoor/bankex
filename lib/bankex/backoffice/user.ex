defmodule Bankex.Backoffice.User do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  schema "backoffice_users" do
    field :email, :string
    field :name, :string
    field :password, :string

    field :old_password, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password])
    |> validate_required([:email, :name, :password])
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 8)
    |> put_password_hash
  end

  @doc false
  def update_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password, :old_password])
    |> validate_required([:email, :name, :password])
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 8)
    |> verify_old_password
    |> put_password_hash
  end

  defp verify_old_password(
         %Ecto.Changeset{valid?: true, changes: %{old_password: old_password}, data: user} =
           changeset
       ) do
    if Argon2.verify_pass(old_password, user.password) do
      change(changeset, old_password: nil)
    else
      add_error(changeset, :old_password, "invalid password")
    end
  end

  defp verify_old_password(
         %Ecto.Changeset{valid?: true, changes: %{password: _password}} = changeset
       ) do
    validate_required(changeset, :old_password)
  end

  defp verify_old_password(changeset), do: changeset

  defp put_password_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset
       ) do
    change(changeset, password: Argon2.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset), do: changeset
end
