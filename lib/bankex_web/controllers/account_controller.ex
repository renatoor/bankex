defmodule BankexWeb.AccountController do
  use BankexWeb, :controller
  use OpenApiSpex.ControllerSpecs

  alias Bankex.Accounts
  alias Bankex.Accounts.UserNotifier

  alias BankexWeb.Schemas.AccountSchema
  alias BankexWeb.Schemas.ErrorSchema

  action_fallback BankexWeb.FallbackController

  tags ["account"]

  operation :index,
    summary: "Get account",
    security: [%{"authorization" => ["read:account"]}],
    responses: [
      ok: {"User info response", "application/json", AccountSchema.AccountResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      conn
      |> put_status(:ok)
      |> render("show.json", account: user.account)
    else
      {:error, :unauthorized}
    end
  end

  operation :withdraw,
    summary: "Withdrawal money from account",
    security: [%{"authorization" => ["write:account"]}],
    request_body:
      {"Withdrawal attributes", "application/json", AccountSchema.Amount, required: true},
    responses: [
      no_content: "Empty response",
      internal_server_error:
        {"Internal server error", "application/json", ErrorSchema.ErrorResponse},
      unprocessable_entity:
        {"Invalid amount error", "application/json", ErrorSchema.ErrorResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def withdraw(conn, %{"amount" => amount}) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      case parse_amount(amount) do
        {:ok, amount} ->
          do_withdraw(conn, user, amount)

        {:error, reason} ->
          conn
          |> put_status(:unprocessable_entity)
          |> render("error.json", error: reason)
      end
    else
      {:error, :unauthorized}
    end
  end

  operation :transfer,
    summary: "Transfer money from account",
    parameters: [
      number: [in: :path, description: "Account number", type: :integer, example: 1]
    ],
    security: [%{"authorization" => ["write:account"]}],
    request_body:
      {"Transfer attributes", "application/json", AccountSchema.Amount, required: true},
    responses: [
      no_content: "Empty response",
      internal_server_error:
        {"Internal server error", "application/json", ErrorSchema.ErrorResponse},
      unprocessable_entity:
        {"Invalid amount error", "application/json", ErrorSchema.ErrorResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def transfer(conn, %{"number" => number, "amount" => amount}) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      if "#{user.account.number}" == number do
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: "transfer_to_own_account")
      else
        case parse_amount(amount) do
          {:ok, amount} ->
            to = Accounts.get_user_by_account!(number)
            do_transfer(conn, user, to, amount)

          {:error, reason} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render("error.json", error: reason)
        end
      end
    else
      {:error, :unauthorized}
    end
  end

  defp parse_amount(amount) when is_binary(amount) do
    case Decimal.parse(amount) do
      {%Decimal{exp: -2, sign: 1} = decimal, ""} ->
        {:ok, decimal |> Decimal.mult(100) |> Decimal.to_integer()}

      _ ->
        {:error, "invalid_amount_format"}
    end
  end

  defp parse_amount(amount) when is_integer(amount) and amount > 0, do: {:ok, amount}

  defp has_credit?(account, amount), do: account.balance - amount > 0

  defp do_withdraw(conn, user, amount) do
    if has_credit?(user.account, amount) do
      case Accounts.withdraw(user.account, amount) do
        {:ok, _} ->
          UserNotifier.deliver_withdraw_notice(user, amount)
          send_resp(conn, :no_content, "")

        {:error, _} ->
          conn
          |> put_status(:internal_server_error)
          |> render("error.json", error: "withdraw_failed")
      end
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render("error.json", error: "insufficient_funds")
    end
  end

  defp do_transfer(conn, from, to, amount) do
    if has_credit?(from.account, amount) do
      case Accounts.transfer(from.account, to.account, amount) do
        {:ok, _} ->
          UserNotifier.deliver_transfer_notice(from, to, amount)
          send_resp(conn, :no_content, "")

        {:error, _} ->
          conn
          |> put_status(:internal_server_error)
          |> render("error.json", error: "transfer_failed")
      end
    else
      conn
      |> put_status(:unprocessable_entity)
      |> render("error.json", error: "insufficient_funds")
    end
  end
end
