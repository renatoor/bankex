defmodule BankexWeb.Backoffice.UserController do
  use BankexWeb, :controller
  use OpenApiSpex.ControllerSpecs

  alias Bankex.Backoffice
  alias Bankex.Backoffice.User

  alias Bankex.Backoffice.Guardian

  alias BankexWeb.Schemas.Backoffice.UserSchema
  alias BankexWeb.Schemas.ErrorSchema

  action_fallback BankexWeb.FallbackController

  tags ["backoffice_user"]

  operation :index,
    summary: "Get user",
    security: [%{"authorization" => ["read:users"]}],
    responses: [
      ok: {"Backoffice user info response", "application/json", UserSchema.UserResponse},
      unauthorized:
        {"Unauthorized backoffice user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      conn
      |> put_status(:ok)
      |> render("show.json", user: user)
    else
      {:error, :unauthorized}
    end
  end

  operation :update,
    summary: "Update user",
    security: [%{"authorization" => ["write:users"]}],
    request_body: {"User attributes", "application/json", UserSchema.Update, required: true},
    responses: [
      ok: {"Backoffice user info response", "application/json", UserSchema.UserResponse},
      unauthorized:
        {"Unauthorized backoffice user error", "application/json", ErrorSchema.ErrorResponse},
      unprocessable_entity: {"Database errors", "application/json", ErrorSchema.ChangesetError}
    ]

  def update(conn, %{"user" => user_params}) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      with {:ok, %User{}} <- Backoffice.update_user(user, user_params) do
        conn
        |> put_status(:ok)
        |> render("show.json", user: user)
      end
    else
      {:error, :unauthorized}
    end
  end

  operation :login,
    summary: "Login",
    request_body: {"Login attributes", "application/json", UserSchema.Login, required: true},
    responses: [
      ok: {"Auth response", "application/json", UserSchema.Auth},
      unauthorized:
        {"Unauthorized backoffice user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def login(conn, %{"email" => email, "password" => password}) do
    case Backoffice.authenticate_user(email, password) do
      {:ok, user} ->
        {:ok, token, _} = Guardian.encode_and_sign(user)

        conn
        |> put_status(:ok)
        |> render("auth.json", token: token)

      {:error, reason} ->
        conn
        |> put_status(:unauthorized)
        |> render("error.json", error: reason)
    end
  end
end
