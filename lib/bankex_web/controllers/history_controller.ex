defmodule BankexWeb.HistoryController do
  use BankexWeb, :controller
  use OpenApiSpex.ControllerSpecs

  alias Bankex.Accounts

  alias BankexWeb.Schemas.HistorySchema
  alias BankexWeb.Schemas.ErrorSchema

  action_fallback BankexWeb.FallbackController

  tags ["history"]

  operation :index,
    summary: "Get history",
    parameters: [
      year: [
        in: :path,
        description: "Year filter",
        type: :integer,
        required: false,
        example: 2021
      ],
      month: [in: :path, description: "Month filter", type: :integer, required: false, example: 1],
      day: [in: :path, description: "Day filter", type: :integer, required: false, example: 1]
    ],
    security: [%{"authorization" => ["read:history"]}],
    responses: [
      ok: {"History response", "application/json", HistorySchema.Log},
      unprocessable_entity:
        {"Invalid parameter error", "application/json", ErrorSchema.ErrorResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def index(conn, %{"year" => year, "month" => month, "day" => day}) do
    with {year, _} <- Integer.parse(year),
         {month, _} <- Integer.parse(month),
         {day, _} <- Integer.parse(day),
         {:ok, begin_date} <- NaiveDateTime.new(year, month, day, 0, 0, 0),
         {:ok, end_date} <- NaiveDateTime.new(year, month, day, 23, 59, 59) do
      filter_by_date(conn, begin_date, end_date)
    else
      :error ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: "invalid_date")
      {:error, reason} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: reason)
    end
  end

  def index(conn, %{"year" => year, "month" => month}) do
    with {year, _} <- Integer.parse(year),
         {month, _} <- Integer.parse(month),
         {:ok, begin_date} <-
           NaiveDateTime.new(Timex.beginning_of_month(year, month), ~T[00:00:00.000]),
         {:ok, end_date} <- NaiveDateTime.new(Timex.end_of_month(year, month), ~T[23:59:59.000]) do
      filter_by_date(conn, begin_date, end_date)
    else
      :error ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: "invalid_date")
      {:error, reason} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: reason)
    end
  end

  def index(conn, %{"year" => year}) do
    with {year, _} <- Integer.parse(year),
         {:ok, begin_date} <- NaiveDateTime.new(Timex.beginning_of_year(year), ~T[00:00:00.000]),
         {:ok, end_date} <- NaiveDateTime.new(Timex.end_of_year(year), ~T[23:59:59.000]) do
      filter_by_date(conn, begin_date, end_date)
    else
      :error ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: "invalid_date")
      {:error, reason} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", error: reason)
    end
  end

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      account = user.account.id
      history = Accounts.get_history(account)
      total_credit = Accounts.get_history_total(account, :credit) || 0
      total_debit = Accounts.get_history_total(account, :debit) || 0

      conn
      |> put_status(:ok)
      |> render("show.json", %{
        history: history,
        total_credit: total_credit,
        total_debit: total_debit
      })
    else
      {:error, :unauthorized}
    end
  end

  defp filter_by_date(conn, begin_date, end_date) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      account = user.account.id
      history = Accounts.get_history_by_date(account, begin_date, end_date)

      total_credit =
        Accounts.get_history_total_by_date(account, :credit, begin_date, end_date) || 0

      total_debit = Accounts.get_history_total_by_date(account, :debit, begin_date, end_date) || 0

      conn
      |> put_status(:ok)
      |> render("show.json", %{
        history: history,
        total_credit: total_credit,
        total_debit: total_debit
      })
    else
      {:error, :unauthorized}
    end
  end
end
