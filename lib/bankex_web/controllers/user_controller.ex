defmodule BankexWeb.UserController do
  use BankexWeb, :controller
  use OpenApiSpex.ControllerSpecs

  alias Bankex.Accounts
  alias Bankex.Accounts.User
  alias Bankex.Accounts.Guardian

  alias BankexWeb.Schemas.UserSchema
  alias BankexWeb.Schemas.ErrorSchema

  action_fallback BankexWeb.FallbackController

  tags ["user"]

  operation :index,
    summary: "Get user",
    security: [%{"authorization" => ["read:users"]}],
    responses: [
      ok: {"User info response", "application/json", UserSchema.UserResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      conn
      |> put_status(:ok)
      |> render("show.json", user: user)
    else
      {:error, :unauthorized}
    end
  end

  operation :create,
    summary: "Create user",
    request_body:
      {"User attributes and agency", "application/json", UserSchema.SignUp, required: true},
    responses: [
      ok: {"User and account response", "application/json", UserSchema.SignUpResponse},
      unprocessable_entity: {"Database errors", "application/json", ErrorSchema.ChangesetError}
    ]

  def create(conn, %{"user" => user_params, "agency" => agency}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params, agency) do
      conn
      |> put_status(:created)
      |> render("sign-up.json", user: user)
    end
  end

  operation :update,
    summary: "Update user",
    security: [%{"authorization" => ["write:users"]}],
    request_body: {"User attributes", "application/json", UserSchema.Update, required: true},
    responses: [
      ok: {"User info response", "application/json", UserSchema.UserResponse},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse},
      unprocessable_entity: {"Database errors", "application/json", ErrorSchema.ChangesetError}
    ]

  def update(conn, %{"user" => user_params}) do
    user = Guardian.Plug.current_resource(conn)

    if user do
      with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
        conn
        |> put_status(:ok)
        |> render("show.json", user: user)
      end
    else
      {:error, :unauthorized}
    end
  end

  operation :login,
    summary: "Login",
    request_body: {"Login attributes", "application/json", UserSchema.Login, required: true},
    responses: [
      ok: {"Auth response", "application/json", UserSchema.Auth},
      unauthorized: {"Unauthorized user error", "application/json", ErrorSchema.ErrorResponse}
    ]

  def login(conn, %{"credential" => credential, "password" => password}) do
    case Accounts.authenticate_user(credential, password) do
      {:ok, user} ->
        {:ok, token, _} = Guardian.encode_and_sign(user)

        conn
        |> put_status(:ok)
        |> render("auth.json", token: token)

      {:error, reason} ->
        conn
        |> put_status(:unauthorized)
        |> render("error.json", error: reason)
    end
  end
end
