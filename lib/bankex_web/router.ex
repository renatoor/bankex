defmodule BankexWeb.Router do
  use BankexWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug Bankex.Accounts.Pipeline
  end

  pipeline :backoffice_auth do
    plug Bankex.Backoffice.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  pipeline :docs do
    plug OpenApiSpex.Plug.PutApiSpec, module: Bankex.ApiSpec
  end

  scope "/" do
    pipe_through :browser

    get "/docs", OpenApiSpex.Plug.SwaggerUI, path: "/api/docs"
  end

  scope "/api" do
    pipe_through [:api, :docs]

    get "/docs", OpenApiSpex.Plug.RenderSpec, []
  end

  scope "/api", BankexWeb do
    pipe_through [:api, :auth]

    scope "/v1" do
      post "/sign-up", UserController, :create
      post "/login", UserController, :login
    end
  end

  scope "/api", BankexWeb do
    pipe_through [:api, :auth, :ensure_auth]

    scope "/v1" do
      get "/user", UserController, :index
      put "/user", UserController, :update
      patch "/user", UserController, :update

      get "/account", AccountController, :index
      post "/account/withdraw", AccountController, :withdraw
      post "/account/transfer/:number", AccountController, :transfer

      get "/account/history", HistoryController, :index
    end
  end

  scope "/api/backoffice", BankexWeb.Backoffice, as: :backoffice do
    pipe_through [:api, :backoffice_auth]

    scope "/v1" do
      post "/login", UserController, :login
    end
  end

  scope "/api/backoffice", BankexWeb.Backoffice, as: :backoffice do
    pipe_through [:api, :backoffice_auth, :ensure_auth]

    scope "/v1" do
      get "/user", UserController, :index
      put "/user", UserController, :update
      patch "/user", UserController, :update

      get "/report", ReportController, :index
    end
  end
end
