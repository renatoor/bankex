defmodule BankexWeb.Schemas.AccountSchema do
  alias OpenApiSpex.Schema

  require OpenApiSpex

  defmodule Account do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Account",
      type: :object,
      properties: %{
        agency: %Schema{type: :integer, description: "Account bank agency"},
        balance: %Schema{type: :string, description: "Account balance"},
        number: %Schema{type: :integer, description: "Account number"},
        digit: %Schema{type: :integer, description: "Account digit"}
      },
      required: [:agency, :balance, :number, :digit],
      example: %{
        "agency" => 1,
        "balance" => "1000.00",
        "number" => 1,
        "digit" => 0
      }
    })
  end

  defmodule AccountResponse do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Account response",
      type: :object,
      properties: %{
        account: Account
      },
      required: [:account],
      example: %{
        "account" => %{
          "agency" => 1,
          "balance" => "1000.00",
          "number" => 1,
          "digit" => 0
        }
      }
    })
  end

  defmodule Amount do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Account operation amount",
      type: :object,
      properties: %{
        amount: %Schema{type: :string, description: "Amount of money"}
      },
      required: [:amount],
      example: %{
        "amount" => "15.00"
      }
    })
  end
end
