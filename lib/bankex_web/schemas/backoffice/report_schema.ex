defmodule BankexWeb.Schemas.Backoffice.ReportSchema do
  alias OpenApiSpex.Schema

  require OpenApiSpex

  defmodule Report do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice report",
      type: :object,
      properties: %{
        total_amount: %Schema{
          type: :string,
          description: "Total amount in the bank at given period"
        },
        total_credit: %Schema{type: :string, description: "Total credited at given period"},
        total_debit: %Schema{type: :string, description: "Total debited at given period"}
      },
      required: [:total_balance, :total_credit, :total_debit],
      example: %{
        "total_amount" => "15.00",
        "total_credit" => "20.00",
        "total_debit" => "5.00"
      }
    })
  end
end
