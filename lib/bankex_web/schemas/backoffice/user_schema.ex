defmodule BankexWeb.Schemas.Backoffice.UserSchema do
  alias OpenApiSpex.Schema

  require OpenApiSpex

  defmodule UserUpdate do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice user update",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "User name", pattern: ~r/[a-zA-Z][a-zA-Z0-9_]+/},
        email: %Schema{type: :string, description: "Email address", format: :email},
        password: %Schema{type: :string, description: "User password"},
        old_password: %Schema{
          type: :string,
          description: "User old password, required to update password"
        }
      },
      example: %{
        "name" => "Joe User",
        "email" => "joe@gmail.com",
        "password" => "********",
        "old_password" => "********"
      }
    })
  end

  defmodule UserInfo do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice user info",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "User name", pattern: ~r/[a-zA-Z][a-zA-Z0-9_]+/},
        email: %Schema{type: :string, description: "Email address", format: :email}
      },
      example: %{
        "name" => "Joe User",
        "email" => "joe@gmail.com"
      }
    })
  end

  defmodule UserResponse do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice user response",
      type: :object,
      properties: %{
        user: UserInfo
      },
      example: %{
        "user" => %{
          "name" => "Joe User",
          "email" => "joe@gmail.com"
        }
      }
    })
  end

  defmodule Update do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice Update user",
      description: "Update user parameters",
      type: :object,
      properties: %{
        user: UserUpdate
      },
      required: [:user],
      example: %{
        user: %{
          "name" => "Joe User",
          "email" => "joe@gmail.com",
          "password" => "********",
          "old_password" => "********"
        }
      }
    })
  end

  defmodule Login do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice Login",
      description: "Login parameters",
      type: :object,
      properties: %{
        email: %Schema{type: :string, description: "User email", format: :email},
        password: %Schema{type: :string, description: "User password", format: :password}
      },
      required: [:email, :password],
      example: %{
        "email" => "joe@gmail.com",
        "password" => "********"
      }
    })
  end

  defmodule Auth do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Backoffice Auth",
      description: "Auth response",
      type: :object,
      properties: %{
        auth: %Schema{type: :string, description: "Authentication token"}
      },
      required: [:auth],
      example: %{
        "auth" => "authentication-token"
      }
    })
  end
end
