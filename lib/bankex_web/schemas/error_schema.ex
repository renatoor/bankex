defmodule BankexWeb.Schemas.ErrorSchema do
  alias OpenApiSpex.Schema

  require OpenApiSpex

  defmodule ErrorResponse do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Error response",
      type: :object,
      properties: %{
        error: %Schema{type: :string, description: "Error"}
      },
      example: %{
        "error" => "message"
      }
    })
  end

  defmodule ChangesetErrorDescription do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Changeset field",
      type: :object,
      properties: %{
        field: %Schema{
          description: "Error list",
          type: :array,
          items: %Schema{type: :string, description: "Error description"}
        }
      },
      required: [:field],
      example: %{
        "field" => [
          "error description"
        ]
      }
    })
  end

  defmodule ChangesetError do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Changeset Error",
      type: :object,
      properties: %{
        errors: ChangesetErrorDescription
      },
      required: [:errors],
      example: %{
        "errors" => %{
          "field" => [
            "error description"
          ]
        }
      }
    })
  end
end
