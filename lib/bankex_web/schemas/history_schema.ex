defmodule BankexWeb.Schemas.HistorySchema do
  alias OpenApiSpex.Schema

  require OpenApiSpex

  defmodule History do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "History",
      type: :object,
      properties: %{
        operation: %Schema{type: :string, description: "Operation name"},
        amount: %Schema{type: :string, description: "Amount of money"},
        previous_balance: %Schema{type: :string, description: "Previous account balance"},
        current_balance: %Schema{type: :string, description: "Current account balance"},
        date: %Schema{type: :string, description: "History date", format: :date}
      },
      required: [:operation, :amount, :previos_balance, :current_balance, :date],
      example: %{
        "operation" => "debit",
        "amount" => "15.00",
        "previous_balance" => "980.00",
        "current_balance" => "965.00",
        "date" => "2021-02-13T10:07:51"
      }
    })
  end

  defmodule Log do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "History log",
      type: :object,
      properties: %{
        history: %Schema{description: "History logs", type: :array, items: History},
        total_credit: %Schema{type: :string, description: "Amount of money credited"},
        total_debit: %Schema{type: :string, description: "Amount of money debited"}
      },
      required: [:history, :total_credit, :total_debit],
      example: %{
        "history" => [
          %{
            "operation" => "debit",
            "amount" => "15.00",
            "previous_balance" => "980.00",
            "current_balance" => "965.00",
            "date" => "2021-02-13T10:07:51"
          }
        ],
        "total_credit" => "1000.00",
        "total_debit" => "35.00"
      }
    })
  end
end
