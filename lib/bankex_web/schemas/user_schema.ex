defmodule BankexWeb.Schemas.UserSchema do
  alias OpenApiSpex.Schema
  alias BankexWeb.Schemas.AccountSchema

  require OpenApiSpex

  defmodule User do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "User schema",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "User name"},
        email: %Schema{type: :string, description: "Email address", format: :email},
        cpf: %Schema{type: :string, description: "User CPF", format: :cpf},
        password: %Schema{type: :string, description: "User password"}
      },
      required: [:name, :email, :cpf, :password],
      example: %{
        "name" => "Joe User",
        "email" => "joe@gmail.com",
        "cpf" => "000.000.000-00",
        "password" => "********"
      }
    })
  end

  defmodule UserUpdate do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "User update schema",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "User name"},
        email: %Schema{type: :string, description: "Email address", format: :email},
        cpf: %Schema{type: :string, description: "User CPF", pattern: :cpf},
        password: %Schema{type: :string, description: "User password"},
        old_password: %Schema{
          type: :string,
          description: "User old password, required to update password"
        }
      },
      example: %{
        "name" => "Joe User",
        "email" => "joe@gmail.com",
        "cpf" => "000.000.000-00",
        "password" => "********",
        "old_password" => "********"
      }
    })
  end

  defmodule UserInfo do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "User info schema",
      type: :object,
      properties: %{
        name: %Schema{type: :string, description: "User name"},
        email: %Schema{type: :string, description: "Email address", format: :email},
        cpf: %Schema{type: :string, description: "User CPF", pattern: ~r/[0-9\.-]+/}
      },
      example: %{
        "name" => "Joe User",
        "email" => "joe@gmail.com",
        "cpf" => "000.000.000-00"
      }
    })
  end

  defmodule UserResponse do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "User response",
      type: :object,
      properties: %{
        user: UserInfo
      },
      example: %{
        "user" => %{
          "name" => "Joe User",
          "email" => "joe@gmail.com",
          "cpf" => "000.000.000-00"
        }
      }
    })
  end

  defmodule SignUp do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Sign-up parameters",
      type: :object,
      properties: %{
        user: User,
        agency: %Schema{type: :integer, description: "Bank agency"}
      },
      required: [:user, :agency],
      example: %{
        user: %{
          "name" => "Joe User",
          "email" => "joe@gmail.com",
          "cpf" => "000.000.000-00",
          "password" => "********"
        },
        agency: 1
      }
    })
  end

  defmodule SignUpResponse do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Sign-up response",
      type: :object,
      properties: %{
        user: UserInfo,
        account: AccountSchema.Account
      },
      required: [:user, :account],
      example: %{
        user: %{
          "name" => "Joe User",
          "email" => "joe@gmail.com",
          "cpf" => "000.000.000-00"
        },
        account: %{
          "agency" => 1,
          "balance" => "1000.00",
          "number" => 1,
          "digit" => 0
        }
      }
    })
  end

  defmodule Update do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Update parameters",
      type: :object,
      properties: %{
        user: UserUpdate
      },
      required: [:user],
      example: %{
        user: %{
          "name" => "Joe User",
          "email" => "joe@gmail.com",
          "cpf" => "000.000.000-00",
          "password" => "********",
          "old_password" => "********"
        }
      }
    })
  end

  defmodule Login do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Login",
      description: "Login parameters",
      type: :object,
      properties: %{
        credential: %Schema{type: :string, description: "User CPF or email"},
        password: %Schema{type: :string, description: "User password"}
      },
      required: [:credential, :password],
      example: %{
        "credential" => "joe@gmail.com",
        "password" => "********"
      }
    })
  end

  defmodule Auth do
    @moduledoc false
    OpenApiSpex.schema(%{
      title: "Auth",
      type: :object,
      properties: %{
        auth: %Schema{type: :string, description: "Authentication token"}
      },
      required: [:auth],
      example: %{
        "auth" => "authentication-token"
      }
    })
  end
end
