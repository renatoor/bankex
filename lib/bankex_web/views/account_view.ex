defmodule BankexWeb.AccountView do
  use BankexWeb, :view
  alias BankexWeb.AccountView

  def render("show.json", %{account: account}) do
    %{account: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: account}) do
    balance = account.balance |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    %{agency: account.agency, number: account.number, digit: account.digit, balance: balance}
  end

  def render("error.json", %{error: reason}) do
    %{error: reason}
  end
end
