defmodule BankexWeb.Backoffice.ReportView do
  use BankexWeb, :view

  def render("show.json", %{total_credit: total_credit, total_debit: total_debit}) do
    total_amount =
      (total_credit - total_debit) |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    total_credit = total_credit |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)
    total_debit = total_debit |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    %{total_amount: total_amount, total_credit: total_credit, total_debit: total_debit}
  end

  def render("error.json", %{error: reason}) do
    %{error: reason}
  end
end
