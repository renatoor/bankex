defmodule BankexWeb.Backoffice.UserView do
  use BankexWeb, :view
  alias BankexWeb.Backoffice.UserView

  def render("sign-up.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{name: user.name, email: user.email}
  end

  def render("auth.json", %{token: token}) do
    %{auth: token}
  end

  def render("error.json", %{error: reason}) do
    %{error: reason}
  end
end
