defmodule BankexWeb.HistoryView do
  use BankexWeb, :view
  alias BankexWeb.HistoryView

  def render("show.json", %{
        history: history,
        total_credit: total_credit,
        total_debit: total_debit
      }) do
    total_credit = total_credit |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)
    total_debit = total_debit |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    %{
      history: render_many(history, HistoryView, "history.json"),
      total_credit: total_credit,
      total_debit: total_debit
    }
  end

  def render("history.json", %{history: history}) do
    amount = history.amount |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    previous_balance =
      history.previous_balance |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    current_balance =
      history.current_balance |> Decimal.new() |> Decimal.div(100) |> Decimal.round(2)

    %{
      operation: history.operation,
      amount: amount,
      previous_balance: previous_balance,
      current_balance: current_balance,
      date: history.inserted_at
    }
  end

  def render("error.json", %{error: reason}) do
    %{error: reason}
  end
end
