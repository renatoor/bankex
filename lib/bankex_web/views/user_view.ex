defmodule BankexWeb.UserView do
  use BankexWeb, :view
  alias BankexWeb.UserView
  alias BankexWeb.AccountView

  def render("sign-up.json", %{user: user}) do
    %{
      user: render_one(user, UserView, "user.json"),
      account: render_one(user.account, AccountView, "account.json")
    }
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{name: user.name, email: user.email, cpf: user.cpf}
  end

  def render("auth.json", %{token: token}) do
    %{auth: token}
  end

  def render("error.json", %{error: reason}) do
    %{error: reason}
  end
end
