defmodule Bankex.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :agency, :integer, null: false
      add :number, :bigserial, null: false
      add :digit, :integer, null: false, default: 0
      add :balance, :integer, null: false
      add :user_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end

    create unique_index(:accounts, [:number])
    create index(:accounts, [:user_id])
  end
end
