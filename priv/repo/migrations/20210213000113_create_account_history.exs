defmodule Bankex.Repo.Migrations.CreateAccountHistory do
  use Ecto.Migration

  def up do
    execute "CREATE TYPE operation AS ENUM ('debit', 'credit')"

    create table(:history) do
      add :operation, :operation, null: false
      add :previous_balance, :integer, null: false
      add :current_balance, :integer, null: false
      add :amount, :integer, null: false
      add :account_id, references(:accounts, on_delete: :nothing), null: false

      timestamps(updated_at: false)
    end

    create index(:history, [:account_id])

    execute """
    CREATE FUNCTION history_log() RETURNS trigger AS $history_log$
      BEGIN

        IF (TG_OP = 'INSERT') THEN
            INSERT INTO
              history (operation, previous_balance, current_balance, amount, account_id, inserted_at)
            VALUES
              ('credit', 0, NEW.balance, NEW.balance, NEW.id, LOCALTIMESTAMP);

            RETURN NEW;
        ELSIF (TG_OP = 'UPDATE') THEN
          IF NEW.balance < OLD.balance THEN
            INSERT INTO
              history (operation, previous_balance, current_balance, amount, account_id, inserted_at)
            VALUES
              ('debit', OLD.balance, NEW.balance, OLD.balance - NEW.balance, NEW.id, LOCALTIMESTAMP);

            RETURN NEW;
          ELSE
            INSERT INTO
              history (operation, previous_balance, current_balance, amount, account_id, inserted_at)
            VALUES
              ('credit', OLD.balance, NEW.balance, NEW.balance - OLD.balance, NEW.id, LOCALTIMESTAMP);

            RETURN NEW;
          END IF;
        END IF;

        RETURN NULL;
      END;
    $history_log$ LANGUAGE plpgsql;
    """

    execute """
    CREATE TRIGGER history_log AFTER INSERT OR UPDATE ON accounts
      FOR EACH ROW EXECUTE PROCEDURE history_log();
    """
  end

  def down do
    execute "DROP TRIGGER history_log ON accounts"

    execute "DROP FUNCTION history_log()"

    drop index(:history, [:account_id])

    drop table(:history)

    execute "DROP TYPE operation"
  end
end
