defmodule Bankex.Repo.Migrations.CreateBackofficeUsers do
  use Ecto.Migration

  def change do
    create table(:backoffice_users) do
      add :email, :text, null: false
      add :name, :text, null: false
      add :password, :text, null: false

      timestamps()
    end

    create unique_index(:backoffice_users, [:email])
  end
end
