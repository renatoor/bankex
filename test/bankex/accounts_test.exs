defmodule Bankex.AccountsTest do
  use Bankex.DataCase

  alias Bankex.Accounts

  describe "users" do
    alias Bankex.Accounts.User

    @password Faker.String.base64(8)

    @new_password Faker.String.base64(8)

    @agency 1

    @valid_user_attrs %{
      cpf: CPF.generate() |> CPF.format(),
      email: Faker.Internet.email(),
      name: Faker.Person.PtBr.name(),
      password: @password
    }

    @valid_other_user_attrs %{
      cpf: CPF.generate() |> CPF.format(),
      email: Faker.Internet.email(),
      name: Faker.Person.PtBr.name(),
      password: @password
    }

    @update_attrs %{
      cpf: CPF.generate() |> CPF.format(),
      email: Faker.Internet.email(),
      name: Faker.Person.PtBr.name(),
      password: @new_password,
      old_password: @password
    }

    @update_wrong_pass %{
      cpf: CPF.generate() |> CPF.format(),
      email: Faker.Internet.email(),
      name: Faker.Person.PtBr.name(),
      password: @password,
      old_password: Faker.String.base64(8)
    }

    @invalid_user_attrs %{cpf: nil, email: nil, name: nil, password: nil}

    def user_fixture(user_attrs) do
      {:ok, user} = Accounts.create_user(user_attrs, @agency)

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture(@valid_user_attrs)
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_user_attrs, @agency)
      assert {:ok, user} == Argon2.check_pass(user, @password, hash_key: :password)
      assert user.cpf == @valid_user_attrs.cpf
      assert user.email == @valid_user_attrs.email
      assert user.name == @valid_user_attrs.name
      assert user.account.agency == @agency
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_user_attrs, @agency)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture(@valid_user_attrs)
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert {:ok, user} == Argon2.check_pass(user, @new_password, hash_key: :password)
      assert user.cpf == @update_attrs.cpf
      assert user.email == @update_attrs.email
      assert user.name == @update_attrs.name
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture(@valid_user_attrs)
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_user_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "update_user/2 with invalid old password returns error changeset" do
      user = user_fixture(@valid_user_attrs)
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @update_wrong_pass)
      assert user == Accounts.get_user!(user.id)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture(@valid_user_attrs)
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end

  describe "accounts" do
    alias Bankex.Accounts.Account

    test "get_account/1 returns the account with given number" do
      user = user_fixture(@valid_user_attrs)
      assert Accounts.get_account!(user.account.number) == user.account
    end

    test "withdraw/2 returns the account with updated balance" do
      user = user_fixture(@valid_user_attrs)

      amount = 500
      balance = user.account.balance

      assert {:ok, %Account{} = account} = Accounts.withdraw(user.account, amount)
      assert account.balance == balance - amount
    end

    test "transfer/3 returns the account with updated balance" do
      user = user_fixture(@valid_user_attrs)
      other_user = user_fixture(@valid_other_user_attrs)

      amount = 500
      balance = user.account.balance
      other_balance = other_user.account.balance

      assert {:ok, %Account{} = other_account} =
               Accounts.transfer(user.account, other_user.account, amount)

      assert other_account.balance == other_balance + amount
      assert Accounts.get_account!(user.account.number).balance == balance - amount
    end

    test "change_account/1 returns a account changeset" do
      user = user_fixture(@valid_user_attrs)
      assert %Ecto.Changeset{} = Accounts.change_account(user.account)
    end
  end

  describe "history" do
    alias Bankex.Accounts.History

    test "get_history/1 returns account history" do
      user = user_fixture(@valid_user_attrs)

      [%History{} = history] = Accounts.get_history(user.account.id)

      assert history.operation == :credit
      assert history.previous_balance == 0
      assert history.current_balance == 100_000
      assert history.amount == 100_000
    end

    test "get_history_total/2 returns history total by operation" do
      user = user_fixture(@valid_user_attrs)

      assert 100_000 = Accounts.get_history_total(user.account.id, :credit)
    end
  end
end
