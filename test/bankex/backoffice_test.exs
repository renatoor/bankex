defmodule Bankex.BackofficeTest do
  use Bankex.DataCase

  alias Bankex.Backoffice

  describe "backoffice_users" do
    alias Bankex.Backoffice.User

    @valid_attrs %{email: "some@email", name: "some name", password: "some password"}
    @update_attrs %{
      email: "some@updated.email",
      name: "some updated name",
      old_password: "some password",
      password: "some updated password"
    }
    @invalid_attrs %{email: nil, name: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Backoffice.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Backoffice.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Backoffice.create_user(@valid_attrs)
      assert {:ok, user} == Argon2.check_pass(user, "some password", hash_key: :password)
      assert user.email == "some@email"
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Backoffice.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Backoffice.update_user(user, @update_attrs)
      assert {:ok, user} == Argon2.check_pass(user, "some updated password", hash_key: :password)
      assert user.email == "some@updated.email"
      assert user.name == "some updated name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Backoffice.update_user(user, @invalid_attrs)
      assert user == Backoffice.get_user!(user.id)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Backoffice.change_user(user)
    end
  end
end
