defmodule BankexWeb.AccountControllerTest do
  use BankexWeb.ConnCase

  alias Bankex.Accounts

  alias Bankex.Accounts.Guardian

  @password Faker.String.base64(8)

  @agency 1

  @valid_amount_string "5.00"

  @invalid_amount_string "1001.00"

  @create_user_attrs %{
    cpf: CPF.generate() |> CPF.format(),
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @password
  }

  @create_other_user_attrs %{
    cpf: CPF.generate() |> CPF.format(),
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @password
  }

  def fixture(:user, user_attrs) do
    {:ok, user} = Accounts.create_user(user_attrs, @agency)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_user]

    test "get account", %{conn: conn, user: user, auth: token} do
      account_number = user.account.number

      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.account_path(conn, :index))

      assert %{
               "agency" => @agency,
               "balance" => "1000.00",
               "digit" => 0,
               "number" => ^account_number
             } = json_response(conn, 200)["account"]
    end

    test "renders errors when user is not authenticated", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer invalid-token")
        |> get(Routes.account_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  describe "withdraw" do
    setup [:create_user]

    test "withdraw money from account with enough funds", %{conn: conn, user: user, auth: token} do
      account_number = user.account.number

      assert conn
             |> put_req_header("authorization", "Bearer #{token}")
             |> post(Routes.account_path(conn, :withdraw), %{"amount" => @valid_amount_string})
             |> response(204)

      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.account_path(conn, :index))

      assert %{
               "agency" => @agency,
               "balance" => "995.00",
               "digit" => 0,
               "number" => ^account_number
             } = json_response(conn, 200)["account"]
    end

    test "renders error when account does not have enough funds", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> post(Routes.account_path(conn, :withdraw), %{"amount" => @invalid_amount_string})

      assert %{"error" => "insufficient_funds"} = json_response(conn, 422)
    end

    test "renders errors when user is not authenticated", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer invalid-token")
        |> get(Routes.account_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  describe "transfer" do
    setup [:create_user, :create_other_user]

    test "transfer money to another account", %{
      conn: conn,
      user: user,
      auth: token,
      other_user: other_user,
      other_auth: other_token
    } do
      account_number = user.account.number
      other_account_number = other_user.account.number

      assert conn
             |> put_req_header("authorization", "Bearer #{token}")
             |> post(Routes.account_path(conn, :transfer, other_user.account.number), %{
               "amount" => @valid_amount_string
             })
             |> response(204)

      user_conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.account_path(conn, :index))

      assert %{
               "agency" => @agency,
               "balance" => "995.00",
               "digit" => 0,
               "number" => ^account_number
             } = json_response(user_conn, 200)["account"]

      other_conn =
        conn
        |> put_req_header("authorization", "Bearer #{other_token}")
        |> get(Routes.account_path(conn, :index))

      assert %{
               "agency" => @agency,
               "balance" => "1005.00",
               "digit" => 0,
               "number" => ^other_account_number
             } = json_response(other_conn, 200)["account"]
    end

    test "renders error when account does not have enough funds", %{
      conn: conn,
      other_user: other_user,
      auth: token
    } do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> post(Routes.account_path(conn, :transfer, other_user.account.number), %{
          "amount" => @invalid_amount_string
        })

      assert %{"error" => "insufficient_funds"} = json_response(conn, 422)
    end

    test "renders error when users transfer to their own account", %{
      conn: conn,
      user: user,
      auth: token
    } do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> post(Routes.account_path(conn, :transfer, user.account.number), %{
          "amount" => @valid_amount_string
        })

      assert %{"error" => "transfer_to_own_account"} = json_response(conn, 422)
    end

    test "renders errors when user is not authenticated", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer invalid-token")
        |> get(Routes.account_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  defp create_user(_) do
    user = fixture(:user, @create_user_attrs)
    {:ok, token, _} = Guardian.encode_and_sign(user)
    %{user: user, auth: token}
  end

  defp create_other_user(_) do
    user = fixture(:user, @create_other_user_attrs)
    {:ok, token, _} = Guardian.encode_and_sign(user)
    %{other_user: user, other_auth: token}
  end
end
