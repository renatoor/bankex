defmodule BankexWeb.Backoffice.UserControllerTest do
  use BankexWeb.ConnCase

  alias Bankex.Backoffice
  alias Bankex.Backoffice.Guardian

  @password Faker.String.base64(8)

  @new_password Faker.String.base64(8)

  @create_attrs %{
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @password
  }

  @update_attrs %{
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @new_password,
    old_password: @password
  }

  @invalid_attrs %{email: nil, name: nil, password: nil}

  def fixture(:user) do
    {:ok, user} = Backoffice.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_user, :auth_user]

    test "lists all backoffice_users", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.backoffice_user_path(conn, :index))

      assert %{
               "email" => email,
               "name" => name
             } = json_response(conn, 200)["user"]

      assert email == @create_attrs.email
      assert name == @create_attrs.name
    end
  end

  describe "update user" do
    setup [:create_user, :auth_user]

    test "renders user when data is valid", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> put(Routes.backoffice_user_path(conn, :update), user: @update_attrs)

      assert %{
               "email" => email,
               "name" => name
             } = json_response(conn, 200)["user"]

      assert email == @create_attrs.email
      assert name == @create_attrs.name
    end

    test "renders errors when data is invalid", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> put(Routes.backoffice_user_path(conn, :update), user: @invalid_attrs)

      assert json_response(conn, 422)
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    %{user: user}
  end

  defp auth_user(%{user: user}) do
    {:ok, token, _} = Guardian.encode_and_sign(user)
    %{auth: token}
  end
end
