defmodule BankexWeb.HistoryControllerTest do
  use BankexWeb.ConnCase

  alias Bankex.Accounts

  alias Bankex.Accounts.Guardian

  @password Faker.String.base64(8)

  @agency 1

  @create_user_attrs %{
    cpf: CPF.generate() |> CPF.format(),
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @password
  }

  def fixture(:user, user_attrs) do
    {:ok, user} = Accounts.create_user(user_attrs, @agency)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_user]

    test "get history", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.history_path(conn, :index))

      assert %{
               "total_credit" => "1000.00",
               "total_debit" => "0.00",
               "history" => [
                 %{
                   "amount" => "1000.00",
                   "previous_balance" => "0.00",
                   "current_balance" => "1000.00",
                   "operation" => "credit"
                 }
               ]
             } = json_response(conn, 200)
    end
  end

  defp create_user(_) do
    user = fixture(:user, @create_user_attrs)
    {:ok, token, _} = Guardian.encode_and_sign(user)
    %{user: user, auth: token}
  end
end
