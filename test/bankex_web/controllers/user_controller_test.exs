defmodule BankexWeb.UserControllerTest do
  use BankexWeb.ConnCase

  alias Bankex.Accounts

  alias Bankex.Accounts.Guardian

  @email Faker.Internet.email()

  @password Faker.String.base64(8)

  @new_password Faker.String.base64(8)

  @agency 1

  @create_user_attrs %{
    cpf: CPF.generate() |> CPF.format(),
    email: @email,
    name: Faker.Person.PtBr.name(),
    password: @password
  }

  @update_user_attrs %{
    cpf: CPF.generate() |> CPF.format(),
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @new_password,
    old_password: @password
  }

  @update_wrong_pass %{
    cpf: CPF.generate() |> CPF.format(),
    email: Faker.Internet.email(),
    name: Faker.Person.PtBr.name(),
    password: @password,
    old_password: Faker.String.base64(8)
  }

  @login_user_attrs %{credential: @email, password: @password}

  @invalid_login_user_attrs %{credential: @email, password: Faker.String.base64(8)}

  @invalid_user_attrs %{cpf: nil, email: nil, name: nil, password: nil}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_user_attrs, @agency)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn =
        post(conn, Routes.user_path(conn, :create), %{user: @create_user_attrs, agency: @agency})

      assert %{
               "user" =>
                 %{
                   "cpf" => cpf,
                   "email" => email,
                   "name" => name
                 } = user,
               "account" =>
                 %{
                   "agency" => agency,
                   "balance" => "1000.00",
                   "digit" => 0
                 } = account
             } = json_response(conn, 201)

      assert cpf == @create_user_attrs.cpf
      assert email == @email
      assert name == @create_user_attrs.name
      assert agency == @agency
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        post(conn, Routes.user_path(conn, :create), %{user: @invalid_user_attrs, agency: @agency})

      assert json_response(conn, 422)
    end
  end

  describe "login user" do
    setup [:create_user]

    test "login user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :login), @login_user_attrs)
      assert json_response(conn, 200)["auth"] != %{}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :login), @invalid_login_user_attrs)
      assert json_response(conn, 401)
    end
  end

  describe "index" do
    setup [:create_user, :auth_user]

    test "get user", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> get(Routes.user_path(conn, :index))

      assert %{
               "cpf" => cpf,
               "email" => email,
               "name" => name
             } = json_response(conn, 200)["user"]

      assert cpf == @create_user_attrs.cpf
      assert email == @email
      assert name == @create_user_attrs.name
    end

    test "renders errors when user is not authenticated", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer invalid-token")
        |> get(Routes.user_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  describe "update user" do
    setup [:create_user, :auth_user]

    test "renders user when data is valid", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> put(Routes.user_path(conn, :update), user: @update_user_attrs)

      assert %{
               "cpf" => cpf,
               "email" => email,
               "name" => name
             } = json_response(conn, 200)["user"]

      assert cpf == @update_user_attrs.cpf
      assert email == @update_user_attrs.email
      assert name == @update_user_attrs.name
    end

    test "renders errors when old password is invalid", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> put(Routes.user_path(conn, :update), user: @update_wrong_pass)

      assert json_response(conn, 422)
    end

    test "renders errors when data is invalid", %{conn: conn, auth: token} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> put(Routes.user_path(conn, :update), user: @invalid_user_attrs)

      assert json_response(conn, 422)
    end

    test "renders errors when user is not authenticated", %{conn: conn} do
      conn =
        conn
        |> put_req_header("authorization", "Bearer invalid-token")
        |> get(Routes.user_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    %{user: user}
  end

  defp auth_user(%{user: user}) do
    {:ok, token, _} = Guardian.encode_and_sign(user)
    %{auth: token}
  end
end
